import { diceCombos, cartesian} from '../diceCombos';

test('boundy conditions', () => {
    let result = diceCombos(2,4,2);
    expect(result).toBe(1);
    result = diceCombos(2,4,1);
    expect(result).toBe(0);
    result = diceCombos(3,4,12);
    expect(result).toBe(1);
    result = diceCombos(3,4,15);
    expect(result).toBe(0);
})

test('generating a cartesian product', () =>{
    let cart = cartesian([1,2], [3,4]);
    expect(cart).toStrictEqual([[1,3], [1,4], [2,3], [2,4]])
})

test('project test case', () => {
    let result = diceCombos(2, 4, 4);
    expect(result).toBe(3);
    result = diceCombos(3, 4, 6);
    expect(result).toBe(10);
}) 