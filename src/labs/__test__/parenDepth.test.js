import parenDepth from '../parenDepth';

test('base test cases', () => {
    let result = parenDepth("( a((b)) ((c)d) )");
    expect(result).toBe(3);
    result = parenDepth("b) (c) ()");
    expect(result).toBe(-1);
    result = parenDepth("(b) (c) ()");
    expect(result).toBe(1);
    result = parenDepth("(b c");
    expect(result).toBe(-1);
    result = parenDepth("who am I and where do I belong");
    expect(result).toBe(0);
})