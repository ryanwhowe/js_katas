import romanNumeral from '../romanNumeral';

test('boundy condition check', () =>{
    let result = romanNumeral(0);
    expect(result).toBe('');
    result = romanNumeral(1001);
    expect(result).toBe('')
})

test('single return value, direct conversion', () => {
    let result = romanNumeral(1);
    expect(result).toBe('I');
    result = romanNumeral(5);
    expect(result).toBe('V');
    result = romanNumeral(10);
    expect(result).toBe('X');
    result = romanNumeral(50);
    expect(result).toBe('L');
})

test('one less than single return value', () => {
    let result = romanNumeral(4);
    expect(result).toBe('IV');
    result = romanNumeral(9);
    expect(result).toBe('IX');
    result = romanNumeral(49);
    expect(result).toBe('XLIX');
    result = romanNumeral(99);
    expect(result).toBe('XCIX');
    result = romanNumeral(999);
    expect(result).toBe('CMXCIX');
})

test('ramdom number tests', () => {
    let result = romanNumeral(4);
    expect(result).toBe('IV');
    result = romanNumeral(7);
    expect(result).toBe('VII');
    result = romanNumeral(43);
    expect(result).toBe('XLIII');
    result = romanNumeral(998);
    expect(result).toBe('CMXCVIII');
})

