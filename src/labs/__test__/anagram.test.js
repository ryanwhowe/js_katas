import annogram from '../anagram'

test('check same string condition', () => {
    let result = annogram('abc','abc')
    expect(result).toBeTruthy();
    result = annogram('a','a')
    expect(result).toBeTruthy();
})

test('check trim and spaces condition', () => {
    let result = annogram(' a b c ','abc')
    expect(result).toBeTruthy();
    result = annogram('a ',' a')
    expect(result).toBeTruthy();
})

test('check anagrams string', () => {
    let result = annogram('cba','abc')
    expect(result).toBeTruthy();
    result = annogram('abcedfghijlkmnop','ponmkljihgfdecba')
    expect(result).toBeTruthy();
    result = annogram('silent','listen')
    expect(result).toBeTruthy();
    result = annogram('silent listen','s i l e n t l i s t e n')
    expect(result).toBeTruthy();
    result = annogram('silent listen',' listen silent ')
    expect(result).toBeTruthy();
})

test('check fun anagrams from internet', () => {
    let result = annogram('Elon Musk','uskmelon')
    expect(result).toBeTruthy();
    result = annogram('Shanghai Nobody','Siobhan Donaghy')
    expect(result).toBeTruthy();
    result = annogram('I am a weakish speller','William Shakespeare')
    expect(result).toBeTruthy();
    result = annogram('true lady','adultery')
    expect(result).toBeTruthy();
    result = annogram('funeral','real fun')
    expect(result).toBeTruthy();
})