import {dateProcessor} from "../dateProcessor";

let data = {
    "2014-01-01": {
        "events": [
            {
                "id": 1,
                "type": 1,
                "description": "Avention Holiday",
                "description_raw": null,
                "date": "2014-01-01"
            },
            {
                "id": 286,
                "type": 94,
                "description": "New Year's Day",
                "description_raw": "New Year's Day",
                "date": "2014-01-01"
            },
            {
                "id": 2,
                "type": 2,
                "description": "PTO",
                "description_raw": null,
                "date": "2014-02-10"
            },
            {
                "id": 451,
                "type": 99,
                "description": "Avention Pay Date",
                "description_raw": null,
                "date": "2008-12-26"
            }
            ,
            {
                "id": 951,
                "type": 95,
                "description": "Station 128 Meeting",
                "description_raw": null,
                "date": "2008-12-26"
            }
        ]
    },
    "2014-01-02": {
        "events": [
            {
                "id": 451,
                "type": 99,
                "description": "Avention Pay Date",
                "description_raw": null,
                "date": "2008-12-26"
            },
            {
                "id": 951,
                "type": 95,
                "description": "Station 128 Meeting",
                "description_raw": null,
                "date": "2008-12-26"
            }
        ]
    },
    "2014-01-03": {
        "events": [
            {
                "id": 1,
                "type": 1,
                "description": "Avention Holiday",
                "description_raw": null,
                "date": "2014-01-01"
            },
            {
                "id": 286,
                "type": 94,
                "description": "New Year's Day",
                "description_raw": "New Year's Day",
                "date": "2014-01-01"
            },
            {
                "id": 2,
                "type": 2,
                "description": "PTO",
                "description_raw": null,
                "date": "2014-02-10"
            }
        ]
    },
    "2014-01-04": {
        "events": [
            {
                "id": 1,
                "type": 1,
                "description": "Avention Holiday",
                "description_raw": null,
                "date": "2014-01-01"
            },
            {
                "id": 286,
                "type": 94,
                "description": "New Year's Day",
                "description_raw": "New Year's Day",
                "date": "2014-01-01"
            }
        ]
    },
    "2014-01-05": {
        "events": [
            {
                "id": 1,
                "type": 1,
                "description": "Avention Holiday",
                "description_raw": null,
                "date": "2014-01-01"
            },
            {
                "id": 2,
                "type": 2,
                "description": "PTO",
                "description_raw": null,
                "date": "2014-02-10"
            }
        ]
    },
};

test('kitchen sink test, all events 1 day', () => {
    let results = dateProcessor(data["2014-01-01"].events);
    let expected = {
        selectable: false,
        css: 'pto paydate fimeeting',
        tooltips: "New Year's Day, Avention Holiday, PTO, Station 128 Meeting, Avention Pay Date"
    };
    expect(results).toStrictEqual(expected);
})

test('Only stackable items included', () => {
    let results = dateProcessor(data["2014-01-02"].events);
    let expected = {
        selectable: true,
        css: 'paydate fimeeting',
        tooltips: "Station 128 Meeting, Avention Pay Date"
    };
    expect(results).toStrictEqual(expected);
})

test('Test primary with all will be PTO priority', () => {

    let results = dateProcessor(data["2014-01-03"].events);
    let expected = {
        selectable: false,
        css: 'pto',
        tooltips: "New Year's Day, Avention Holiday, PTO"
    };
    expect(results).toStrictEqual(expected);
})

test('Test primary with only holidays will be company as priority', () => {

    let results = dateProcessor(data["2014-01-04"].events);
    let expected = {
        selectable: false,
        css: 'companyholiday',
        tooltips: "New Year's Day, Avention Holiday"
    };
    expect(results).toStrictEqual(expected);
})

test('Non national Holiday should be selectable', () => {

    let results = dateProcessor(data["2014-01-05"].events);
    let expected = {
        selectable: true,
        css: 'pto',
        tooltips: "Avention Holiday, PTO"
    };
    expect(results).toStrictEqual(expected);
})