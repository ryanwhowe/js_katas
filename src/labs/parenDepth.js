/**
 * This week's question:
Find and return the maximum depth of parenthesis given a string. If the parenthesis are unbalanced, return -1.

Example:
parenDepth("( a((b)) ((c)d) )")
> 3
parenDepth("b) (c) ()")
> -1
parenDepth("who am I and where do I belong")
> 0
 */

const parenDepth = (string)  => {
    let max = 0,
    current = 0;

    string.split('').forEach(element => {
        if(element === '(')current++;
        if(element === ')') if (current-- === 0) return;
        if(current > max) max = current;
    });
    return (current === 0) ? max : -1;
}

export default parenDepth;
