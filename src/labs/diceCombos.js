
/**
 * This week's question:
Given N number of dice with F number of faces each (numbered from 1 to F), find the number of ways to get a given sum X.

Example:
> diceCombos(n = 2, f = 4, x = 4)
> 3 // (1, 3), (2, 2), (3, 1)
 */
const diceCombos = (dice, faces, total) => {
    // boundry conditions
    if(dice < 1) return 0;
    if(faces < 4) return 0;
    if(total < dice) return 0;
    if(total > dice * faces) return 0;
    if(total === dice) return 1;
    if(total === dice * faces) return 1;

    let faceValues = [...Array(faces).keys()].map(x => ++x);
    let die=[...Array(dice).keys()].map(x => x=faceValues);
    let test = cartesian(...die);
    let reducer = (result, current) => result += total===current.reduce((a,b) => a+b) ? 1 : 0;
    return test.reduce(reducer, 0);
}

const cartesian = (...a) => a.reduce((a,b) => a.map(x => b.map(y=> x.concat(y))).reduce((a,b) => a.concat(b),[]), [[]]);

export { diceCombos, cartesian };