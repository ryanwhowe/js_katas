
/**
 * return if the two string are anagrams of each other or not
 * 
 * @param {string} s1 
 * @param {string} s2 
 */
const annogram = (s1, s2) =>{
    if(s1.trim() === s2.trim()) return true;
    const trimSort = (s) => s.toLowerCase().trim().split(" ").join("").split("").sort().join("");
    return (trimSort(s1) === trimSort(s2));
}

export default annogram;