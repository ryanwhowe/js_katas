
const dateProcessor = (events) => {
    let selectable = events.reduce(function(val, event,) { return (val ? (event.type !== 94) : val);}, true);
    let css = events.reduce(getStackableCss, events.reduce(getPrimaryCss,''));
    let tooltips = events.reduce(collectTooltips, [])
    return {selectable: selectable, css: css.trim(), tooltips: tooltips.filter((el) => el != null).join(', ')};
}

const getPrimaryCss = (css, event) => {
    if(css === 'pto') return css;
    if(event.type === 2) return 'pto'
    if(event.type === 3) return 'sick'
    if(css === 'sick') return css;
    if(event.type === 1) return 'companyholiday'
    if(css === 'companyholiday') return css;
    if(event.type === 94) return 'nationalholiday'
    return css;
}

const getStackableCss = (css, event) => {
    if(event.type === 99) return css + ' paydate';
    if(event.type === 95) return css + ' fimeeting';
    return css
};

const mapTypeToPriority = (type) => {
    if(type === 94) return 1;
    if(type === 1) return 2;
    if(type === 2) return 3;
    if(type === 3) return 4;
    if(type === 95) return 5;
    if(type === 99) return 6;
}

const collectTooltips = (tooltips, event) => {
    let type = mapTypeToPriority(event.type);
    tooltips[type] = event.description;
    return tooltips;
}

export { dateProcessor }